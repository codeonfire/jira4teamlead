// Initial welcome page. Delete the following line to remove it.
'use strict';

// const dom = require('../lib/dom');
const isDevelopment = process.env.NODE_ENV !== 'production'
import path from 'path';
// // import Vue from 'vue';

// dom.append.stylesheet(`${isDevelopment ? '' : '/assets'}/css/normalize.css`);
// dom.append.stylesheet(`${isDevelopment ? '' : '/assets'}/css/skeleton.css`);
// dom.append.stylesheet(`https://fonts.googleapis.com/css?family=Raleway`);
// dom.append.script('https://unpkg.com/vue').addEventListener('load', init);
import Vue from 'vue'
import App from './App'
import {ipcRenderer} from 'electron';

Vue.config.productionTip = true

let app = new Vue(App);
app.$mount('#app');

ipcRenderer.on('updateReport',(e,report)=>{
    app.updateReport(report)
})

ipcRenderer.on('loading.started',()=>{
    app.loading=true;
});

ipcRenderer.on('loading.done',()=>{
    app.loading=false;
});

