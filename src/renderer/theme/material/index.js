// Initial welcome page. Delete the following line to remove it.
'use strict';

const isDevelopment = process.env.NODE_ENV !== 'production'

import path from 'path';
import Vue from 'vue'
import VueMaterial from 'vue-material';
import App from './App'
import dom from '../../../lib/dom';
import { ipcRenderer } from 'electron';

dom.append.stylesheet(`${isDevelopment ? '' : 'assets/'}/css/vue-material.css`);
dom.append.stylesheet(`https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic`);
dom.append.stylesheet(`https://fonts.googleapis.com/css?family=Material+Icons`);

Vue.config.productionTip = true
Vue.use(VueMaterial);


let app = new Vue(App);
app.$mount('#app');

ipcRenderer.on('updateReport', (e, report) => {
    app.updateReport(report)
})

ipcRenderer.on('updateProjects', (e, projects) => {
    app.updateProjects(projects)
})

ipcRenderer.on('updateBoards', (e, boards) => {
    console.log({boards})
    app.updateBoards(boards)
})

ipcRenderer.on('loading.started', () => {
    app.loading = true;
});

ipcRenderer.on('loading.done', () => {
    app.loading = false;
});

ipcRenderer.on('modal.show', () => {
    app.openDialog('settings');
});

ipcRenderer.on('modal.hide', () => {
    app.closeDialog('settings');
});

