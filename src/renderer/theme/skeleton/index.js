// Initial welcome page. Delete the following line to remove it.
'use strict';

const isDevelopment = process.env.NODE_ENV !== 'production'

import path from 'path';
import Vue from 'vue'
import App from './App'
import dom from '../../../lib/dom';
import { ipcRenderer } from 'electron';

dom.append.stylesheet(`${isDevelopment ? '' : 'assets/'}/css/normalize.css`);
dom.append.stylesheet(`${isDevelopment ? '' : 'assets/'}/css/skeleton.css`);
dom.append.stylesheet(`${isDevelopment ? '' : 'assets/'}/css/font-awesome.css`);
dom.append.stylesheet(`https://fonts.googleapis.com/css?family=Raleway`);

Vue.config.productionTip = true


let app = new Vue(App);
app.$mount('#app');

ipcRenderer.on('updateReport', (e, report) => {
    app.updateReport(report)
})

ipcRenderer.on('loading.started', () => {
    app.loading = true;
});

ipcRenderer.on('loading.done', () => {
    app.loading = false;
});

ipcRenderer.on('modal.show', () => {
    app.showModal = true;
});

ipcRenderer.on('modal.hide', () => {
    app.showModal = false;
});

