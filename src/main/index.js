'use strict'

import electron from 'electron';
const { app, BrowserWindow, Tray, ipcMain, powerSaveBlocker, shell, globalShortcut } = electron;

import path from 'path';
import {updateConfig, getConfig} from '../lib/conf';
import fetch from 'node-fetch';
import { Jira } from '../lib/jira';

let config=getConfig();

powerSaveBlocker.start('prevent-app-suspension');
app.dock.hide();

const isDevelopment = process.env.NODE_ENV !== 'production'

// Global reference to mainWindow
// Necessary to prevent win from being garbage collected
let mainWindow;

if (!isDevelopment) app.setLoginItemSettings({ openAtLogin: true })

function createMainWindow() {
  let appIcon;
  if (isDevelopment) {
    appIcon = path.join(__static, '/appIcon/appIcon.png');
  } else {
    appIcon = path.resolve(__dirname, 'assets/appIcon/appIcon.png');
  }
  
  // Construct new BrowserWindow
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize;;
  const window = new BrowserWindow({
    width: config.width,
    height: height,
    x: width - config.width,
    y: 0,
    frame: false,
    show: false,
    icon: appIcon,
    alwaysOnTop: config.alwaysOnTop,
  })
  window.setVisibleOnAllWorkspaces(true);
  window.setSkipTaskbar(true);

  // points to `webpack-dev-server` in development
  // points to `index.html` in production
  const url = isDevelopment
    ? `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`
    : `file://${__dirname}/index.html`

  // DevTools in Dev mode
  if (isDevelopment) {
    window.webContents.openDevTools()
  }

  window.loadURL(url)

  window.on('closed', () => {
    mainWindow = null
  })

  window.webContents.on('devtools-opened', () => {
    window.focus()
    setImmediate(() => {
      window.focus()
    })
  })

  return window
}

// Quit application when all windows are closed
app.on('window-all-closed', () => {
  // On macOS it is common for applications to stay open
  // until the user explicitly quits
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', () => {
  // On macOS it is common to re-create a window
  // even after all windows have been closed
  if (mainWindow === null) mainWindow = createMainWindow()
})


// interferes with modal
// app.on('web-contents-created', function (event, wc) {
//   wc.on('before-input-event', function (event, input) {
//     if (input.key === 'Escape' && !input.ctrl && !input.alt && !input.meta && !input.shift) {
//       event.preventDefault()
//       mainWindow.hide();
//     }
//   })
// })

// Create main BrowserWindow when electron is ready
app.on('ready', () => {
  // Jira Instance
  
  // TrayIcon
  let trayIcon;
  if (isDevelopment) {
    trayIcon = path.join(__static, '/trayIcon/trayIcon.png');
  } else {
    trayIcon = path.resolve(__dirname, 'assets/trayIcon/trayIcon.png');
  }
  let tray = new Tray(trayIcon);
  
  mainWindow = createMainWindow();
  
  tray.on('click', (e) => {
    let trayPoints = tray.getBounds();
    let curDisplay = electron.screen.getDisplayNearestPoint({ x: trayPoints.x, y: trayPoints.y });
    let workArea = curDisplay.workArea;
    
    mainWindow.setBounds({
      x: trayPoints.x,
      y: workArea.y,
      width: Math.min(config.width,workArea.width),
      height: workArea.height,
    });
    // mainWindow.maximize();
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
  })
  mainWindow.on('show', () => {
    tray.setHighlightMode('always')
    getReport();
  })
  mainWindow.on('hide', () => {
    tray.setHighlightMode('never')
  })
  
  mainWindow.on('blur', () => {
    if (!isDevelopment) mainWindow.hide();
  })
  
  function getProjects(){
    try{
      const conf=getConfig().jira;
      const jira = new Jira(conf.host, conf.user, conf.password);      
      jira.getProjects().then(projects=>{
        mainWindow.webContents.send('updateProjects',projects);
      })
    }catch(e){
      console.log('HERE');
    }
  }
  
  function getBoards(project){
    try{
      const conf=getConfig().jira;
      console.log('getting boards');
      const jira = new Jira(conf.host, conf.user, conf.password);      
      jira.getAllBoards(project).then(boards=>{
        mainWindow.webContents.send('updateBoards',boards);
      }).catch(console.log)
    }catch(e){
      console.log('HERE');
    }
  }

  function getReport(){
    const conf=getConfig();
    getProjects();
    getBoards(conf.project);
    try{
      const jira = new Jira(conf.jira.host, conf.jira.user, conf.jira.password, conf.jira.boardId);
      mainWindow.webContents.send('loading.started');
      jira.getCurrentSprintTickets((report)=>{      
        mainWindow.webContents.send('updateReport', report)
        mainWindow.webContents.send('loading.done');
      },(err)=>{
        console.log('here');
        mainWindow.show();
        mainWindow.webContents.send('modal.show');
        mainWindow.webContents.send('loading.done');
        mainWindow.webContents.send('updateReport', {})
      });
    }catch(e){
      console.log('HERE');
    }
  }
  
  getReport();
  setInterval(getReport, config.refreshRate);
  ipcMain.on('requestUpdate', function() {
    getReport()
  })
  
  ipcMain.on('showTicket', function(e, ticket) {
    console.log({ ticket });
    shell.openExternal(`https://spreeza.atlassian.net/browse/${ticket}`)
  })  

  ipcMain.on('getBoards', function(e) {
    config=getConfig();
    if(config.project){
      getBoards(config.project)
    }
  })  
  // Shortcut
  
  globalShortcut.register('CmdOrCtrl+Esc', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
  })

  globalShortcut.register('CmdOrCtrl+,', () => {
    mainWindow.webContents.send('modal.show');
  })


})
