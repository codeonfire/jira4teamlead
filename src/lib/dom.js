/* global document HTMLElement Element  */

// Element.matches Polyfill
if (Element && !Element.prototype.matches) {
    var proto = Element.prototype;
    proto.matches = proto.matchesSelector ||
        proto.mozMatchesSelector || proto.msMatchesSelector ||
        proto.oMatchesSelector || proto.webkitMatchesSelector;
}


const dom = {
    isElem(e) {
        return e instanceof HTMLElement;
    },

    create(tag, classes, attributes, content) {
        let elem = document.createElement(tag);
        elem.className = classes;
        for (let attribute in attributes) {
            elem.setAttribute(attribute, attributes[attribute]);
        }
        if (content) {
            if (content instanceof HTMLElement) {
                elem.appendChild(content);
            } else {
                elem.innerHTML = content;
            }
        }
        return elem;
    },

    queryAll(selector, elem) {
        elem = elem instanceof HTMLElement ? elem : document;

        const toArray = function(coll, fromIndex) {
            return Array.prototype.slice.call(coll, fromIndex);
        };

        return toArray(elem.querySelectorAll(selector));
    },

    query(selector, elem) {
        let ret = this.queryAll(selector, elem);
        return ret.length > 0 ? ret[0] : null;
    },

    getClosest(elem, selector) {
        // Get closest match
        for (; elem && elem !== document; elem = elem.parentNode) {
            if (elem.matches(selector)) {
                return elem;
            }
        }
        return false;
    },

    getClassList(elem) {
        return elem.className.split(' ');
    },

    setClassList(elem, classList) {
        if (!Array.isArray(classList)) throw new Error('setClassList requires classList to be an Array');
        elem.className = classList.join(' ');
    },

    removeClass(elem, classNames) {
        classNames = classNames.split(' ');
        this.setClassList(elem, this.getClassList(elem).filter(cls => !~classNames.indexOf(cls)));
    },

    addClass(elem, classNames) {
        classNames = classNames.split(' ');
        this.setClassList(elem, this.getClassList(elem).filter(cls => !~classNames.indexOf(cls)).concat(classNames));
    },

    hasClass(elem, className) {
        if (!elem) {
            return false;
        }

        return ~this.getClassList(elem).indexOf(className);
    },

    toggleClass(elem, classNames) {
        classNames = classNames.split(' ');
        classNames.forEach(cls => {
            this[this.hasClass(elem, cls) ? 'removeClass' : 'addClass'](elem, cls);
        });
    },

    hasClassPos(elem, className) {
        if (!elem) {
            return false;
        }

        return this.getClassList(elem).indexOf(className);
    },

    makeTag(tag, attribs, content) {
        let el = document.createElement(tag);
        if (content) el.innerText = content;
        if (attribs instanceof Object) {
            for (prop in attribs) {
                el.setAttribute(prop, attribs[prop]);
            }
        }
        return el;
    },

    append(parent, tag, attribs, content) {
        let el = this.makeTag(tag, attribs, content)
        if (!this.isElem(parent)) {
            parent = this.query(parent);
        }
        if (this.isElem(parent)) {
            parent.appendChild(el);
        }
        return el;
    }

};

dom.append.style = (content) => {
    return dom.append(document.head, 'style', {}, content);
};

dom.append.stylesheet = (href) => {
    return dom.append(document.head, 'link', {rel:'stylesheet', type:'text/css', href}, null);
};

dom.append.script = (src, content = undefined) => {
    return dom.append(document.head, 'script', { type: "text/javascript", src }, content);
};


module.exports = dom;
