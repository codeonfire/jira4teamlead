import fetch from 'node-fetch';

const ISSUE_KEY_MAP={
    'customfield_10004':'estimate'
};

export class Jira {
    constructor(hostname, username, password, boardId) {
        this.baseUrl = `https://${hostname}`
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.boardId = boardId;
        this.credentials = new Buffer(`${this.username}:${this.password}`).toString('base64');
        this.header = { Authorization: `Basic ${this.credentials}`, 'Content-Type': 'application/json' }
        this.data = {};
    }

    _get(url) {
        // return fetch(`${this.baseUrl}${url}`, { method: 'get', headers: this.header }).then(res => res.text()).then(console.log)
        return fetch(`${this.baseUrl}${url}`, { method: 'get', headers: this.header }).then(res => { if (res.status !== 200) throw new Error('not authenticated'); return res; }).then(res => res.json()).catch(e => { throw e; })
    }

    _post(url, data) {
        return fetch(`${this.baseUrl}${url}`, { method: 'post', headers: this.header, body: JSON.stringify(data) }).then(res => res.json())
    }

    getProjects(){
        return this._get(`/rest/api/2/project?recent=10`)
    }

    getAllBoards(project){
        return this._get(`/rest/agile/1.0/board?maxResults=100&projectKeyOrId=${project}`).then(res=>res.values)
    }

    getSprintList() {
        return this._get(`/rest/greenhopper/1.0/sprintquery/${this.boardId}`).then(sprints => { this.data.sprints = sprints; return sprints });
    }

    getCurrentSprint() {
        return this.getSprintList().then(sprintList => { this.data.currentSprint = sprintList.sprints.pop(); return this.data.currentSprint; })
    }

    getCurrentSprintTickets(callback, ecallback) {
        return this.getCurrentSprint().then(sprint => this._get(`/rest/agile/1.0/board/${this.boardId}/sprint/${sprint.id}/issue?maxResults=500&fields=customfield_10004,priority,issuetype,subtasks,flagged,assignee,status,summary`))
            .then(res=>{
                res.issues.forEach(issue=>{
                    for(let fieldName in issue.fields){
                        let field=issue.fields[fieldName];
                            if(ISSUE_KEY_MAP.hasOwnProperty(fieldName)){
                                issue.fields[ISSUE_KEY_MAP[fieldName]]=field;
                            }
                    }
                })
                return res;
            })
            .then(res => {
                let tickets = res.issues.map(issue => {
                    issue.fields.key = issue.key;
                    issue.fields.self = issue.self;
                    issue.fields.id = issue.id;
                    return issue.fields;
                });

                tickets = tickets.map(ticket => {
                    ticket.subtasks = ticket.subtasks.map(subtask => {
                        subtask.fields.key=subtask.key;
                        subtask.fields.self=subtask.self;
                        subtask.fields.id=subtask.id;
                        if(tickets.find(ticket => { return ticket.key === subtask.key })===null){
                            tickets.push(subtask.fields);
                        }
                        return subtask.fields;
                    })
                    return ticket;
                })

                return {
                    updatedAt: Date.now(),
                    tickets
                }
            })
            .then(report => {
                report.sprintList=this.data.sprints;
                this.sprintReport = report;
                if (typeof callback === 'function') {
                    callback(report);
                }
            })
            .catch(ecallback)

    }

}
