// import { app } from 'electron';
import fs from 'fs';
import path from 'path';

const confFile = path.resolve('/tmp/', 'j4t.config.json')
const defaultConfig = {
    jira: {
        host: 'spreeza.atlassian.net',
        version: '2',
        greenHopperApiVersion: '1.0',
        boardId: '63'
    },
    refreshRate: 5 * 60 * 1000,
    width: 1100,
    alwaysOnTop: true
};

export function updateConfig(conf) {
    fs.writeFileSync(confFile, JSON.stringify(conf))
}


export function getConfig(){
    if (!fs.existsSync(confFile)) {
        updateConfig(defaultConfig);
        return defaultConfig;
    }else{
        return JSON.parse(fs.readFileSync(confFile))
    }
}

export default getConfig()
