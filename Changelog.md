# Changelog

### 0.4.4
 - Bumped up max fetched items

### 0.4.3
 - Added Icons
 - Estimates
 - Better Subtask display
 - Subtasks Assigned to Me

### 0.4.2
 - Added 'QA Passed' status to 'Awaiting Release'

### 0.4.1
 - Removed Reload button

### 0.4.0
 - Material design
 - My Focus Section
 - Selectable project and board
 - Renamed to Jira4Teams

### 0.3.1
 - On conn error -- show window and settings
 
### 0.3.0
 - Display Subtasks
 - Layout Update

### 0.2.6
 - Autorefresh on open
 - Settings Pane

### 0.2.5
 - Filter out Subtasks

### 0.2.4
 - Remove from dock on osx

### 0.2.3
 - Visible on all workspaces
 - AlwaysOnTop
 - Shortcut for toggle changes to CMD+Esc
 - ESC key closes window
 - Remove from taskbar

### 0.2.2
 - CMD+` toggle key

### 0.2.1
 - Better Layout and styling
 - Some code-cleanup



